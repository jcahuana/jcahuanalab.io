jQuery(function($){

  var Background = (function($){

    // DOM using jQuery
    var dom = {};

    // Eventos que tendrá cada elemento
    var suscribeEvents = function() {};


    /**
     * Objeto que guarda métodos que se van a usar en cada evento definido en
     * 'suscribeEvents'
     */
    var events = {};

    // Función que inicializará las funciones descritas anteriormente
    var initialize = function() {

      $.backstretch([
        "img/code-1.jpg",
        "img/code-2.jpg"
      ], {duration: 3000, fade: 750});
    };

    /**
     * Retorna uin objeto literal con el método de inicialización principal,
     */
    return {
      start: initialize
    };

  })(jQuery);

  // Ejecutamos el módulo
  Background.start();

});
